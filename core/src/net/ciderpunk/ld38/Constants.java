package net.ciderpunk.ld38;

/**
 * Created by Matt on 22/04/2017.
 */
public class Constants {
  public static String KeyPreferencesPath = "config.keys";

  public static final int BlockSize = 16;
  public static final float TileSize = 32f;
  public static final String AtlasPath = "res.atlas";
  public static final float TimeStep = 1/60f;
  public static final float DefaultGravity = -12f;



  public static final float BulletSpeed = 30f;
  public static final float BulletDeviation = 5f;


  public static final  float PlayerMaxVelocity = 8f;
  public static final  float PlayerGroundHorizontalImpulse =1f;

  public static final  float PlayerAirHorizontalImpulse =0.5f;
  public static final float PlayerJumpImpulse = 10f;
  public static final float PlayerFriction = 10f;

  public static final float PlayerHealth = 100f;

  public static final float BulletTimeToLive = 4f;
  public static final float PlayerShootDelay = 0.1f;
  public static final float PlayerBulletDamage = 10f;
  public static final float PlayerBulletSpeed = BulletSpeed;

  public static final float PlayerBulletDeviation = BulletDeviation;


  public static final short World_Fixture = 1;
  public static final short Player_Fixture = 2;
  public static final short Enemy_Fixture = 4;
  public static final short Bullet_Fixture = 8;

}
