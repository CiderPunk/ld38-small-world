package net.ciderpunk.ld38;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import net.ciderpunk.ld38.controls.ActionMap;
import net.ciderpunk.ld38.resources.interfaces.IResourceWatcher;
import net.ciderpunk.ld38.resources.ResourceManager;
import net.ciderpunk.ld38.ui.*;

public class Ld38Game extends ApplicationAdapter implements IResourceWatcher, net.ciderpunk.ld38.ui.interfaces.IGameManager {

  public static final  ActionMap keymap  = new ActionMap();
  GameView game;
	SplashScreen splash;
	Loader loader;
	net.ciderpunk.ld38.ui.interfaces.IView currentView;
	public final ResourceManager resman;//  = new ResourceManager(this);
	boolean ready;

	public ResourceManager getResMan(){
	  return resman;
	}


	public Ld38Game(){
    super();
    resman = new ResourceManager(this);
  }

	@Override
	public void create () {
		//resman = new ResourceManager(this);
		game = new GameView( this);
		splash = new SplashScreen(this);
    loader = new Loader(this);
    resman.addResourceUser(loader);
		resman.addResourceUser(splash);
		resman.addResourceUser(game);
		setView(game);
	}

	@Override
	public void render () {
		if (resman.update()) {
			currentView.Draw();
      currentView.Update(Gdx.graphics.getDeltaTime());

    }
    else{
		  loader.Draw();
    }
	}
	
	@Override
	public void dispose () {
		game.dispose();
		splash.dispose();
	}

	@Override
	public void preLoad(ResourceManager resMan) {

	}

	@Override
	public void postLoad(ResourceManager resMan) {

	}

	@Override
	public void loadComplete() {
		ready = true;
    keymap.loadConfig(Constants.KeyPreferencesPath);
  }

	@Override
	public void SwitchView(Views view) {
		if (ready){
			switch(view){
				case  Game:
				  setView(game);
					break;
				case Splash:
				  setView(splash);
					break;
			}
		}
	}

	public void setView(net.ciderpunk.ld38.ui.interfaces.IView view){
	  if (currentView != null){
	    currentView.CleanUp();
    }
    currentView = view;
	  currentView.Init();
  }

}
