package net.ciderpunk.ld38.controls.interfaces;

import net.ciderpunk.ld38.controls.Action;

/**
 * Created by matthewlander on 13/10/15.
 */
public interface IActionConsumer {

	public boolean startAction(Action action);
	public boolean endAction(Action action);
}
