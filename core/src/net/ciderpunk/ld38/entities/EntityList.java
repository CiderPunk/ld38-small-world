package net.ciderpunk.ld38.entities;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.PooledLinkedList;
import net.ciderpunk.ld38.entities.interfaces.IEntity;
import net.ciderpunk.ld38.entities.interfaces.ISpriteEntity;

/**
 * Created by matthewlander on 18/09/15.
 */
public class EntityList {

	protected PooledLinkedList<IEntity> mainBuffer;
	protected PooledLinkedList<IEntity> addBuffer;

	public EntityList(int mainBufferSize, int addBufferSize) {
		addBuffer = new PooledLinkedList<IEntity>(addBufferSize);
		mainBuffer = new PooledLinkedList<IEntity>(mainBufferSize);
	}

	public EntityList() {
		this(1000, 100);
	}

	public void clear() {
		IEntity ent;
		mainBuffer.iter();
		while ((ent = mainBuffer.next()) != null) {
			mainBuffer.remove();
			ent.cleanUp();
		}
		addBuffer.iter();
		while ((ent = addBuffer.next()) != null) {
			addBuffer.remove();
			ent.cleanUp();
		}

	}

	protected void consolidateList() {
		IEntity ent;
		addBuffer.iter();
		while ((ent = addBuffer.next()) != null) {
			mainBuffer.add(ent);
			addBuffer.remove();
		}
	}

	/**
	 * Performs update on all members of the list
	 *
	 * @param dT delta time
	 */
	public EntityList doUpdate(float dT) {
		this.consolidateList();
		IEntity ent;
		mainBuffer.iter();
		while ((ent = mainBuffer.next()) != null) {
			if (!ent.update(dT)) {
				mainBuffer.remove();
				ent.cleanUp();
			}
		}
		return this;
	}

	/**
	 * perform draw operation on all members of the list
	 *
	 * @param batch
	 */
	public void doDraw(SpriteBatch batch) {
		IEntity ent;
		mainBuffer.iter();
		while ((ent = mainBuffer.next()) != null) {
      if (ent instanceof ISpriteEntity) {
        ((ISpriteEntity)ent).draw(batch);
      }
		}
	}
/*
  public void doDraw(ShapeRenderer shaper) {
    IEntity ent;
    mainBuffer.iter();
    while ((ent = mainBuffer.next()) != null) {
      if (ent instanceof IShapeEntity) {
        ((IShapeEntity)ent).draw(shaper);
      }
    }
  }


  public void doDraw(ModelBatch batch, Environment env) {
    IEntity ent;
    mainBuffer.iter();
    while ((ent = mainBuffer.next()) != null) {
      if (ent instanceof IModelEntity) {
        ((IModelEntity)ent).doDraw(batch, env);
      }
    }
  }
*/

	public void add(IEntity arg0) {
		addBuffer.add(arg0);
	}

	public void remove(IEntity target) {
		IEntity ent;
		mainBuffer.iter();
		while ((ent = mainBuffer.next()) != null) {
			if (ent == target) {
				mainBuffer.remove();
				break;
			}
		}
	}

  public static final Vector2 temp = new Vector2();

  public IEntity FindNearest(Vector2 target){
    mainBuffer.iter();
    IEntity ent;
    IEntity closest = null;
    float bestDist = Float.MAX_VALUE;
    while ((ent = mainBuffer.next()) != null) {
      float dist = ent.getLoc(temp).sub(target).len();
      if (dist < bestDist){
        bestDist = dist;
        closest = ent;
      }
    }
    return closest;
  }

}
