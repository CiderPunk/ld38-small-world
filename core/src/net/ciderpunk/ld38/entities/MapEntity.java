package net.ciderpunk.ld38.entities;

import com.badlogic.gdx.math.Vector2;
import net.ciderpunk.ld38.entities.interfaces.IEntity;
import net.ciderpunk.ld38.level.Level;

/**
 * Created by Matt on 22/04/2017.
 */
public abstract class MapEntity implements IEntity {
  public  Vector2 position;
  protected boolean flip;
  protected final Level level;

  public MapEntity(Level owner, float x, float y, boolean flip){
    position = new Vector2(x,y);
    this.level = owner;
    this.flip = flip;
  }


  @Override
  public Vector2 getLoc(Vector2 loc) {
    return position;
  }
}
