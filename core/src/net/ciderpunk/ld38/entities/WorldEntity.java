package net.ciderpunk.ld38.entities;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import net.ciderpunk.ld38.level.Level;

/**
 * Created by Matt on 22/04/2017.
 */
abstract public class WorldEntity extends MapEntity{
  protected Body body;

  public WorldEntity(Level owner, float x, float y, World world, boolean flip) {
    super(owner, x, y, flip);
    addToWorld(world);
  }

  protected abstract void addToWorld(World world);

  @Override
  public boolean update(float dT) {
    this.position.set( body.getPosition());
    return true;
  }

}
