package net.ciderpunk.ld38.entities.blufor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import net.ciderpunk.ld38.Constants;
import net.ciderpunk.ld38.Ld38Game;
import net.ciderpunk.ld38.controls.Action;
import net.ciderpunk.ld38.controls.interfaces.IActionConsumer;
import net.ciderpunk.ld38.entities.WorldEntity;
import net.ciderpunk.ld38.entities.interfaces.IDamageSource;
import net.ciderpunk.ld38.entities.interfaces.ISpriteEntity;
import net.ciderpunk.ld38.entities.missile.Bullet;
import net.ciderpunk.ld38.gfx.AnimationBuilder;
import net.ciderpunk.ld38.gfx.Frame;
import net.ciderpunk.ld38.level.Level;
import net.ciderpunk.ld38.resources.ResourceManager;
import net.ciderpunk.ld38.resources.interfaces.IResourceUser;

import static net.ciderpunk.ld38.entities.blufor.Player.PlayerMove.jump;

/**
 * Created by Matt on 22/04/2017.
 */
public class Player extends WorldEntity implements ISpriteEntity, IActionConsumer, IDamageSource{

  static Frame gun;
  static Frame head;
  static Frame torsoStanding;
  static Frame torsoJump;
  static Animation torsoWalk;

  protected int actions = 0;
  Vector2 vel = new Vector2();
  Fixture sensor ;
  boolean isOnGround = false;
  protected Frame torsoeFrame;

  float gunAngle = 0f;
  float headAngle = 0f;
  float shootCoolDown = 0f;
  final GroundTestRayCastCallBack  groudRayCallBack;
  float shootTime = 0f;
  float walkTime= 0f;


  public Player(Level owner, float x, float y, World world, boolean flip) {
    super(owner, x, y, world, flip);
    actionDown.addConsumer(this);
    actionUp.addConsumer(this);
    actionLeft.addConsumer(this);
    actionRight.addConsumer(this);
    actionJump.addConsumer(this);
    actionShoot.addConsumer(this);

    groudRayCallBack = new GroundTestRayCastCallBack(this);
    torsoeFrame = torsoStanding;
  }

  @Override
  public void cleanUp() {

  }


  static final Vector2 recoil = new Vector2();


  @Override
  public void draw(SpriteBatch batch) {
    torsoeFrame.draw(batch,this.position.x, this.position.y, flip);
    head.draw(batch, this.position.x + (0.05f * (flip ? 1: -1)), this.position.y+0f, flip, headAngle);
    recoil.set(0.1f,0f).rotate(flip ? -gunAngle:gunAngle).scl((float) Math.abs(Math.sin(shootTime * 30f)));
    gun.draw(batch, this.position.x + (0.2f * (flip ? 1: -1)) + recoil.x , this.position.y-0.3f + recoil.y, flip, gunAngle);
  }

  @Override
  protected void addToWorld(World world) {
    BodyDef bodyDef = new BodyDef();
// We set our body to dynamic, for something like ground which doesn't move we would set it to StaticBody
    bodyDef.type = BodyDef.BodyType.DynamicBody;
// Set our body's starting position in the world
    bodyDef.position.set(position);

    body = world.createBody(bodyDef);
    float[] shapeVerts = {-0.45f, -0.6f, 0.45f, -0.6f, 0.5f, 0.7f,-0.5f, 0.7f };
    PolygonShape playerShape = new PolygonShape();
    playerShape.set(shapeVerts);
    // Create a fixture definition to apply our shape to
    FixtureDef fixtureDef = new FixtureDef();
    fixtureDef.shape = playerShape;
    fixtureDef.density =0.5f;
    fixtureDef.friction = 0;
    fixtureDef.filter.categoryBits = Constants.Player_Fixture;
    Fixture bodyfixture = body.createFixture(fixtureDef);
    bodyfixture.setUserData(this);
    playerShape.dispose();


    CircleShape circle = new CircleShape();
    circle.setRadius(0.45f);
    circle.setPosition(new Vector2(0, -0.55f));

    FixtureDef wheeldef = new FixtureDef();
    wheeldef.shape = circle;
    wheeldef.density = 0.5f;
    wheeldef.friction = Constants.PlayerFriction;
    wheeldef.restitution = 0f;
    wheeldef.filter.categoryBits = Constants.Player_Fixture;
    sensor = body.createFixture(wheeldef);
    sensor.setUserData(this);
    circle.dispose();


    body.setFixedRotation(true);
  }

  protected static final class GroundTestRayCastCallBack implements RayCastCallback{
    public boolean grounded = false;
    protected final Player owner;

    public GroundTestRayCastCallBack(Player player) {
      owner = player;
    }

    @Override
    public float reportRayFixture(Fixture fixture, Vector2 point, Vector2 normal, float fraction) {
      Object target = fixture.getUserData();

      if (target instanceof Bullet){
        return -1;
      }
      if (target != owner) {
        grounded = true;
        return 0;
      }
      else {
        return -1;
      }
    }
  }

  protected boolean isOnGround(){
    return rayGroundTest(-0.45f) || rayGroundTest(+0.45f);
  }

  protected boolean rayGroundTest(float xoffs){
    groudRayCallBack.grounded = false;
    body.getWorld().rayCast(groudRayCallBack, this.position.x+xoffs, this.position.y+-0.8f, this.position.x+xoffs, this.position.y - 1.1f);
    return groudRayCallBack.grounded;
  }

 protected boolean isActive(PlayerMove move){
    return PlayerMove.isActive(move, actions);
 }

  final static Vector2 bvel = new Vector2();
  final static Vector2 bpos = new Vector2();


  @Override
  public boolean update(float dT) {
    super.update(dT);
    isOnGround = isOnGround();
    //Gdx.app.log("on ground",String.valueOf(isOnGround));
    vel.set(body.getLinearVelocity());

    //correct player direction
    if (Math.abs(vel.x) > 0.1f)
      flip = vel.x < 0;

    if (isActive(PlayerMove.left) && isActive(PlayerMove.right)){}
    else if (isActive(PlayerMove.left)){
      if (vel.x > -Constants.PlayerMaxVelocity) {
        body.applyLinearImpulse(isOnGround ? -Constants.PlayerGroundHorizontalImpulse : -Constants.PlayerAirHorizontalImpulse, 0, position.x, position.y, true);
      }
    }
    else if (isActive(PlayerMove.right)){
      if (vel.x < Constants.PlayerMaxVelocity) {
        body.applyLinearImpulse(isOnGround ? Constants.PlayerGroundHorizontalImpulse : Constants.PlayerAirHorizontalImpulse, 0, position.x, position.y, true);
      }
    }

    if (isActive(PlayerMove.up) && isActive(PlayerMove.down)){}
    else if (isActive(PlayerMove.up)){
      //gunAngle = (float) (-0.5f * Math.PI);
      gunAngle =45f;
      headAngle = 20f;
    }
    else if (isActive(PlayerMove.down)){
      //gunAngle = (float) (0.5f * Math.PI);
      gunAngle = -45f;
      headAngle = -20f;
    }
    else{
      //gunAngle++;
      gunAngle = 0f;
      headAngle = 0f;
    }

    if (isActive(PlayerMove.shoot)){
      if (shootCoolDown < 0){
        bvel.set(Constants.PlayerBulletSpeed, 0).rotate((flip ? 180 - gunAngle : gunAngle) + (((float)Math.random() - 0.5f) * Constants.PlayerBulletDeviation)).add(vel);
        bpos.set(position).add(0,-0.3f);
        this.level.addBullet(true, bpos, bvel, Constants.PlayerBulletDamage, this);
        shootCoolDown = Constants.PlayerShootDelay;
      }
      shootTime += dT;
    }
    shootCoolDown-=dT;

    //pick an animation frame for the torso

      //TODO addd jump anim

    torsoeFrame = torsoStanding;
    if (isOnGround){
      if (Math.abs(vel.x) >0.2f){
        walkTime+=dT;
        torsoeFrame = (Frame) torsoWalk.getKeyFrame(walkTime, true);
      }
      else {
        walkTime = 0;
      }
    }
    else{
      torsoeFrame = torsoJump;

    }
    return true;
  }

  @Override
  public boolean startAction(Action action) {
    PlayerMove act  = ((PlayerAction) action).move;
    actions = PlayerMove.addAction(actions,act);
    switch (act){
      case jump:
        if (isOnGround){
          body.applyLinearImpulse(0,Constants.PlayerJumpImpulse,position.x,position.y ,true);
          //Gdx.app.log("jump", "is on ground");
        }
        break;
    }
    return true;
  }

  @Override
  public boolean endAction(Action action) {
    PlayerMove act  = ((PlayerAction) action).move;
    actions = PlayerMove.subAction(actions, act);
    switch (act){
      case jump:
        if (isOnGround){
          body.applyLinearImpulse(0,Constants.PlayerJumpImpulse,position.x,position.y ,true);
          //Gdx.app.log("jump", "is on ground");
        }
        break;
      case shoot:
        shootTime = 0f;
    }
    return true;
  }

  public static final class Loader implements IResourceUser {
    @Override
    public void preLoad(ResourceManager resMan) {
      resMan.load(Constants.AtlasPath, TextureAtlas.class);
    }

    @Override
    public void postLoad(ResourceManager resMan) {
      TextureAtlas atlas = resMan.getAssetMan().get(Constants.AtlasPath, TextureAtlas.class);
      torsoStanding = new Frame(atlas.findRegion("torso"),20, 48, 1/48f, Color.WHITE);

      torsoJump = new Frame(atlas.findRegion("jump"),20, 52, 1/48f, Color.WHITE);

      torsoWalk  = AnimationBuilder.buildAnim(atlas, "torso", 0.1f, 1/48f, new int[]{0,1,0,2}, new float[]{20f,48f, 20f,51f, 20f,48f ,25f,52f});
      head = new Frame(atlas.findRegion("head"),24, 16, 1/48f, Color.WHITE);
      gun = new Frame(atlas.findRegion("gun"),16, 40, 1/48f, Color.WHITE);
    }
  }

  //define actions
  protected static final PlayerAction actionJump = new PlayerAction("Jump", "p1_jump", Input.Keys.Z, jump);
  protected static final PlayerAction actionShoot = new PlayerAction("Shoot", "p1_shoot", Input.Keys.X, PlayerMove.shoot);
  protected static final PlayerAction actionLeft = new PlayerAction("Left", "p1_left", Input.Keys.LEFT, PlayerMove.left);
  protected static final PlayerAction actionRight = new PlayerAction("Right", "p1_right", Input.Keys.RIGHT, PlayerMove.right);
  protected static final PlayerAction actionUp = new PlayerAction("Up", "p1_up",Input.Keys.UP, PlayerMove.up);
  protected static final PlayerAction actionDown = new PlayerAction("Down", "p1_down",Input.Keys.DOWN, PlayerMove.down);

  //register actions
  static{
    Ld38Game.keymap.registerActions(new Action[]{ actionJump, actionShoot, actionLeft, actionRight, actionUp, actionDown });
  }

  protected static class PlayerAction extends Action {
    public final PlayerMove move;
    public PlayerAction(String name, String key, int defKey, PlayerMove move) {
      super(name, key, defKey);
      this.move = move;
    }
  }

  ///enumeration of possible player actions
  protected enum PlayerMove{
    jump(1),
    shoot(2),
    left(4),
    right(8),
    up(16),
    down(32);

    public final int mask;
    PlayerMove(int mask){
      this.mask = mask;
    }

    public static int addAction(int val, PlayerMove action){
      val = val | action.mask;
      return val;
    }

    public static int subAction(int val, PlayerMove action){
      val = val ^ action.mask;
      return val;
    }
    public static boolean isActive(PlayerMove move, int actions){
      return ((actions | move.mask)  == actions);
    }
  }

}
