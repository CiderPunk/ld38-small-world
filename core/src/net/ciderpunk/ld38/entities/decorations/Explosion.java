package net.ciderpunk.ld38.entities.decorations;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Pool;
import net.ciderpunk.ld38.Constants;
import net.ciderpunk.ld38.entities.MapEntity;
import net.ciderpunk.ld38.entities.interfaces.IEntity;
import net.ciderpunk.ld38.entities.interfaces.ISpriteEntity;
import net.ciderpunk.ld38.gfx.AnimationBuilder;
import net.ciderpunk.ld38.gfx.Frame;
import net.ciderpunk.ld38.level.Level;
import net.ciderpunk.ld38.resources.ResourceManager;
import net.ciderpunk.ld38.resources.interfaces.IResourceUser;

/**
 * Created by Matt on 23/04/2017.
 */
public class Explosion implements ISpriteEntity {

  final Vector2 position = new Vector2();
  static Animation explodeAnim;
  float time, speed, scale, rot;
  Frame currentFrame;


  private static final Pool<Explosion> pool = new Pool<Explosion>(){
    @Override
    protected Explosion newObject() {
      return new Explosion();
    }
  };

  public Explosion() {  }

  public static Explosion getExplosion(Vector2 position, float scale, float speed){
    return getExplosion(position.x, position.y, scale, speed);
  }

  public static Explosion getExplosion(float x, float y, float scale, float speed){
    return pool.obtain().init(x, y,scale, speed);
  }

  private Explosion init(float x, float y, float scale, float speed) {
    position.set(x,y);
    time = 0f;
    this.speed = speed;
    this.scale = scale;
    rot = (float) (Math.random() * 360);
    currentFrame = (Frame) explodeAnim.getKeyFrame(0);
    return this;
  }


  @Override
  public void cleanUp() {

    pool.free(this);
  }

  @Override
  public boolean update(float dT) {
    time+=dT;
    float adjusted = time * speed;
    currentFrame = (Frame) explodeAnim.getKeyFrame(adjusted);
    return !explodeAnim.isAnimationFinished(adjusted);
  }

  @Override
  public Vector2 getLoc(Vector2 loc) {
    return position;
  }


  @Override
  public void draw(SpriteBatch batch) {
    currentFrame.draw(batch, position.x, position.y, rot, scale);
  }

  public static class Loader implements IResourceUser {

    @Override
    public void preLoad(ResourceManager resMan) {
      resMan.load(Constants.AtlasPath, TextureAtlas.class);
    }

    @Override
    public void postLoad(ResourceManager resMan) {
      explodeAnim =  AnimationBuilder.buildAnim(resMan.get(Constants.AtlasPath, TextureAtlas.class), "explode", 0.02f, 25, 25,0.1f);
    }
  }
}
