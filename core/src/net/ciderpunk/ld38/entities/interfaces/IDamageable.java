package net.ciderpunk.ld38.entities.interfaces;

/**
 * Created by Matt on 23/04/2017.
 */
public interface IDamageable {

  void hurt(float damage, IDamageSource source);

}
