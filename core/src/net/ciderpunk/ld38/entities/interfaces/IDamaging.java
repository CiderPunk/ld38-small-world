package net.ciderpunk.ld38.entities.interfaces;

import com.badlogic.gdx.physics.box2d.Contact;

/**
 * Created by Matt on 23/04/2017.
 */
public interface IDamaging {
  void hit(Contact contact, Object target);
}
