package net.ciderpunk.ld38.entities.interfaces;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by matthewlander on 18/09/15.
 */
public interface IEntity {
	void cleanUp();
	boolean update(float dT);
  Vector2 getLoc(Vector2 loc);
  void draw(SpriteBatch batch);
//	void debugDraw(ShapeRenderer renderer);
}
