package net.ciderpunk.ld38.entities.interfaces;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by Matthew on 12/01/2016.
 */
public interface ISpriteEntity extends IEntity {
  void draw(SpriteBatch batch);
}
