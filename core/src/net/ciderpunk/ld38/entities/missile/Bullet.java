package net.ciderpunk.ld38.entities.missile;


import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.utils.Pool;
import net.ciderpunk.ld38.Constants;
import net.ciderpunk.ld38.entities.interfaces.IDamageSource;
import net.ciderpunk.ld38.entities.interfaces.IDamageable;
import net.ciderpunk.ld38.entities.interfaces.IDamaging;
import net.ciderpunk.ld38.entities.interfaces.ISpriteEntity;
import net.ciderpunk.ld38.gfx.Frame;
import net.ciderpunk.ld38.resources.ResourceManager;
import net.ciderpunk.ld38.resources.interfaces.IResourceUser;

public class Bullet implements Pool.Poolable, ISpriteEntity, IDamaging {

	static Frame bulletFrame;

  final Body body;
  final BulletPool pool;
  protected boolean alive;
  protected float ttl;
  private float damage;
  private IDamageSource source;

  protected static float TimeToLive = 4f;

  protected Bullet(World world, Shape shape, BulletPool pool, short maskBits){
    this.pool = pool;
    BodyDef bodyDef = new BodyDef();
    bodyDef.type = BodyDef.BodyType.DynamicBody;
    bodyDef.awake = false;
    body = world.createBody(bodyDef);
    body.setGravityScale(0.1f);
    FixtureDef fixtureDef = new FixtureDef();
    fixtureDef.shape = shape;
    fixtureDef.density = 0.1f;
    fixtureDef.filter.categoryBits = Constants.Bullet_Fixture;
    fixtureDef.filter.maskBits = maskBits;
    body.createFixture(fixtureDef);
    body.setUserData(this);
    body.setBullet(true);
    alive = false;
  }


  protected Bullet init(Vector2 pos, Vector2 velocity, float damage, IDamageSource source) {
    this.damage = damage;
    body.setTransform(pos,0f);
    body.setLinearVelocity(velocity);
    body.setAwake(true);
    body.setActive(true);
    alive = true;
    ttl = Constants.BulletTimeToLive;
    this.source = source;
    return this;
  }

  @Override
	public void cleanUp() {
    body.setAwake(false);
    body.setActive(false);
    pool.free(this);
    alive = false;
	}

	@Override
	public boolean update(float dT) {
    ttl -= dT;
    return alive && ttl > 0;
	}

  @Override
  public Vector2 getLoc(Vector2 loc) {
    return body.getWorldPoint(loc);
  }

  @Override
	public void draw(SpriteBatch batch){
    bulletFrame.draw(batch, body.getPosition().x, body.getPosition().y);

	}

	@Override
	public void reset() {

	}

  public void hit(Contact contact, Object target) {
    if (target == null){
      //hit a wall
    }
    else{
      if (target instanceof IDamageable){
        ((IDamageable)target).hurt(damage, source);
      }
    }
    if (alive)
      pool.owner.addExplosion(body.getPosition(), 0.01f, 1f);

    alive = false;

  }

  public static class Loader implements IResourceUser {

		@Override
		public void preLoad(ResourceManager resMan) {
			resMan.load(Constants.AtlasPath, TextureAtlas.class);
		}

		@Override
		public void postLoad(ResourceManager resMan) {
			bulletFrame = new Frame(resMan.getAssetMan().get(Constants.AtlasPath, TextureAtlas.class).findRegion("bullet"),3,3, 0.05f);
		}
	}


}
