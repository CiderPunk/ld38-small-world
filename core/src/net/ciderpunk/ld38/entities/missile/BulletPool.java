package net.ciderpunk.ld38.entities.missile;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.Pool;
import net.ciderpunk.ld38.entities.interfaces.IDamageSource;
import net.ciderpunk.ld38.level.Level;

/**
 * Created by Matthew on 15/01/2016.
 */
public class BulletPool extends Pool<Bullet> implements Disposable {
  public final Level owner;
  final CircleShape circle;
  final short mask;
  final World world;

  public BulletPool(Level owner, World world,  short maskBits){
    super(20);
    this.owner = owner;
    this.world =  world;
    circle = new CircleShape();
    circle.setRadius(0.1f);
    this.mask = maskBits;
  }

  @Override
  protected Bullet newObject() {
    return new Bullet(world, circle, this, mask);
  }

  public Bullet getBullet(Vector2 pos, Vector2 velocity, float damage, IDamageSource source){
    return this.obtain().init(pos, velocity, damage, source);
  }

  @Override
  public void dispose() {
    circle.dispose();
  }
}