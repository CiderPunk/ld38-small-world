package net.ciderpunk.ld38.level;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.objects.TiledMapTileMapObject;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import net.ciderpunk.ld38.Constants;
import net.ciderpunk.ld38.entities.EntityList;
import net.ciderpunk.ld38.entities.EntityTypes;
import net.ciderpunk.ld38.entities.blufor.Player;
import net.ciderpunk.ld38.entities.decorations.Explosion;
import net.ciderpunk.ld38.entities.interfaces.IDamaging;
import net.ciderpunk.ld38.entities.missile.BulletPool;
import net.ciderpunk.ld38.resources.interfaces.IResourceUser;
import net.ciderpunk.ld38.resources.ResourceManager;
import net.ciderpunk.ld38.ui.GameView;
import net.ciderpunk.ld38.level.interfaces.ILevel;

/**
 * Created by Matt on 22/04/2017.
 */
public class Level implements ILevel, IResourceUser {


  EntityList bluFor;
  EntityList opFor;
  EntityList missiles;
  EntityList decoration;


  Player player;

  TiledMap map;
  String mapPath;
  final GameView owner;
  OrthogonalTiledMapRenderer mapRenderer;
  Box2DDebugRenderer debugRenderer;

  Vector2 size;

  OrthographicCamera cam;
  World world;
  float accumulator = 0;


  BulletPool blueBulletPool, opForBulletPool;


  public Level(String path, GameView view ){
    this.mapPath = path;
    this.owner = view;


    bluFor = new EntityList();
    opFor = new EntityList();
    missiles = new EntityList();
    decoration = new EntityList();


    world = new World(new Vector2(0f, Constants.DefaultGravity), true);

    blueBulletPool = new BulletPool(this, world, (short)( Constants.World_Fixture | Constants.Enemy_Fixture) );
    opForBulletPool = new BulletPool(this, world, (short)( Constants.World_Fixture | Constants.Player_Fixture) );


    world.setContactListener(new ContactListener() {
      @Override
      public void beginContact(Contact contact) {
        Object obj1 = contact.getFixtureA().getBody().getUserData();
        Object obj2 = contact.getFixtureB().getBody().getUserData();
        if (obj1 instanceof IDamaging){
          ((IDamaging)obj1).hit(contact, obj2);
        }
        if (obj2 instanceof IDamaging){
          ((IDamaging)obj2).hit(contact, obj1);
        }
/*
        //sensor events
        if (obj1 instanceof LevelEvent){
          ((LevelEvent) obj1).trigger(obj2);
        }
        if (obj2 instanceof LevelEvent){
          ((LevelEvent) obj2).trigger(obj1);
        }
        */
      }
      @Override
      public void endContact(Contact contact) {
      }
      @Override
      public void preSolve(Contact contact, Manifold oldManifold) {
      }


      @Override
      public void postSolve(Contact contact, ContactImpulse impulse) {

      /*
        Object obj1 = contact.getFixtureA().getBody().getUserData();
        Object obj2 = contact.getFixtureB().getBody().getUserData();
        if (obj1 instanceof IEntity){
          ((Entity)obj1).collision(contact, impulse, obj2);
        }
        if (obj2 instanceof ICollidable){
          ((Entity)obj2).collision(contact, impulse, obj1);
        }
        */
      }
    });

  }


  public void update(float dT) {
    bluFor.doUpdate(dT);
    missiles.doUpdate(dT);
    decoration.doUpdate(dT);
    cam.position.set(player.position,0f );

    float frameTime = Math.min(dT, 0.25f);
    accumulator += frameTime;
    while (accumulator >= Constants.TimeStep) {
      world.step(Constants.TimeStep,6,2);
      accumulator -= Constants.TimeStep;
    }
  }

  public void draw(SpriteBatch batch, boolean debugView) {
    cam.update();
    mapRenderer.setView(cam);
    mapRenderer.render();


    batch.setProjectionMatrix(cam.combined);
    batch.begin();


    missiles.doDraw(batch);
    bluFor.doDraw(batch);
    decoration.doDraw(batch);
    batch.end();


    if (debugView)
      debugRenderer.render(world, cam.combined);
  }

  @Override
  public void preLoad(ResourceManager resMan) {
    resMan.load(mapPath,TiledMap.class);
  }

  @Override
  public void postLoad(ResourceManager resMan) {
    map = resMan.get(mapPath, TiledMap.class);
    cam = new OrthographicCamera();
    cam.setToOrtho(false, 30,20);
    cam.update();
    mapRenderer = new OrthogonalTiledMapRenderer(map,  1/Constants.TileSize);
    mapRenderer.setView(cam);
    debugRenderer = new Box2DDebugRenderer();
    MapProperties props = map.getProperties();
    size= new Vector2(props.get("width", int.class), props.get("height", int.class));

    for (MapLayer layer : map.getLayers()){
      if (layer.getName().equals("ents")){
        //entity layer!
        createEntities(layer);
      }
      if (layer.getName().equals("solid")){
        createFixtures(layer);
      }
    }

  }

  private void createFixtures(MapLayer layer) {

    PolygonShape shape = new PolygonShape();

    TiledMapTileLayer tileLayer = (TiledMapTileLayer) layer;
    int columns = tileLayer.getWidth();
    int rows = tileLayer.getHeight();

    int xblocks= (int) Math.ceil(columns / Constants.BlockSize);
    int yblocks = (int) Math.ceil(rows / Constants.BlockSize);


    for(int y = 0; y < yblocks; y++){
      for(int x = 0; x < xblocks; x++) {
        createBlockBody(tileLayer, x * Constants.BlockSize,y * Constants.BlockSize, Math.max((x  + 1)* Constants.BlockSize, rows),Math.max((y+1) * Constants.BlockSize, columns), shape );
      }
    }
    shape.dispose();
  }

  static final Vector2 vect = new Vector2();


  private void createBlockBody(TiledMapTileLayer tileLayer, int minX, int minY, int maxX, int maxY, PolygonShape shape) {
    BodyDef blockDef = new BodyDef();
    blockDef.position.set(minX, minY);
    Body body = world.createBody(blockDef);
    int rowLen = 0;
    for (int y = 0; y < (maxX - minY); y++) {
      for (int x = 0; x < (maxX - minX); x++) {
        TiledMapTileLayer.Cell cell = tileLayer.getCell(x + minX, y+minY);
        if (cell != null){
          rowLen++;
        }
        else {
          createRowFixture(x-1,y, rowLen, body, shape);
          rowLen = 0;
        }
      }
      createRowFixture(maxX - minX,y, rowLen, body, shape);
      rowLen = 0;
    }

  }


  protected void createRowFixture(int endX, int endY, int len, Body bod, PolygonShape shape){
    if (len > 0) {
      float hw = (float)len / 2f;
      shape.setAsBox(hw, 0.5f, vect.set(endX -hw +1, endY + 0.5f), 0f);
      Fixture fx = bod.createFixture(shape, 1f);


    }
  }



  //create map entites from objects
  public void createEntities(MapLayer layer){
    for(MapObject obj : layer.getObjects()){
      if (obj instanceof TiledMapTileMapObject) {
        final TiledMapTile tile = ((TiledMapTileMapObject) obj).getTile();
        final String ent = tile.getProperties().get("ent", "IGNORE", String.class).toUpperCase();
        float x= ((TiledMapTileMapObject) obj).getX() / Constants.TileSize;
        float y= ((TiledMapTileMapObject) obj).getY() / Constants.TileSize;
        boolean flip = ((TiledMapTileMapObject) obj).isFlipHorizontally();
        //try get
        try {
          EntityTypes et = EntityTypes.valueOf(ent);
          switch (et) {
            case PLAYER:
              player = new Player(this,x,y,world,flip);
              bluFor.add(player);

              //cam.translate(x-30,y-20);

              break;
          }
        } catch (IllegalArgumentException ex) {


        }
      }
    }
  }


  @Override
  public void dispose() {
    if (mapRenderer != null) mapRenderer.dispose();
    if (debugRenderer != null) debugRenderer.dispose();
    if (world!= null) world.dispose();

    //try remove from resman.... maybe
  }


  public void addExplosion(Vector2 pos, float scale, float speed){
   addExplosion(pos.x,pos.y,scale,speed);
  }

  public void addExplosion(float x, float y, float scale, float speed){
    decoration.add(Explosion.getExplosion(x,y,scale,speed));
  }

  public void addBullet(boolean blufor, Vector2 position, Vector2 velocity, float damage, Player source) {
    missiles.add(
            blufor ?
                    blueBulletPool.getBullet(position, velocity, damage, source) :
                    opForBulletPool.getBullet(position, velocity, damage, source)
    );
  }
}
