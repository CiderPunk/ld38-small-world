package net.ciderpunk.ld38.level.interfaces;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Disposable;
import net.ciderpunk.ld38.resources.interfaces.IResourceUser;


/**
 * Created by Matt on 22/04/2017.
 */
public interface ILevel extends Disposable, IResourceUser {

  void draw(SpriteBatch batch, boolean debugView);

  void update(float dT);

}
