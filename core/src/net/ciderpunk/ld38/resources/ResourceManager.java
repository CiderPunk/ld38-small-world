package net.ciderpunk.ld38.resources;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.PooledLinkedList;

public class ResourceManager implements Disposable {

	AssetManager assetMan;
	PooledLinkedList<net.ciderpunk.ld38.resources.interfaces.IResourceUser> postLoadList;
	boolean loading;
	net.ciderpunk.ld38.resources.interfaces.IResourceWatcher owner;

	public AssetManager getAssetMan() {
		return assetMan;
	}

  public void load(String filename, Class type){
    assetMan.load(filename, type);
  }

  public <T> T get(String filename, Class<T> type){
    return assetMan.get(filename, type);
  }

	public ResourceManager(net.ciderpunk.ld38.resources.interfaces.IResourceWatcher owner){
		assetMan = new AssetManager();
		assetMan.setLoader(TiledMap.class, new TmxMapLoader(new InternalFileHandleResolver()));
		postLoadList = new PooledLinkedList<net.ciderpunk.ld38.resources.interfaces.IResourceUser>(100);
		addResourceUser(owner);
		this.owner = owner;
	}

	public boolean update(){
		if (loading){
			if (assetMan.update()) {
				loading = false;
				postLoadList.iterReverse();
				net.ciderpunk.ld38.resources.interfaces.IResourceUser item;
				while((item = postLoadList.previous()) !=null) {
					item.postLoad(this);
					postLoadList.remove();
				}
				owner.loadComplete();
			}
			return false;
		}
		return true;
	}

	public boolean isLoading(){
		return loading;
	}
	
	public void dispose(){
		assetMan.dispose();
	}
	
	public void addResourceUser(net.ciderpunk.ld38.resources.interfaces.IResourceUser object){
		postLoadList.add(object);
		object.preLoad(this);
		loading = true;
	}
	
	public void addResourceUser(net.ciderpunk.ld38.resources.interfaces.IResourceUser[] objects){
		for(net.ciderpunk.ld38.resources.interfaces.IResourceUser object : objects){
			addResourceUser(object);
		}
	}

}
