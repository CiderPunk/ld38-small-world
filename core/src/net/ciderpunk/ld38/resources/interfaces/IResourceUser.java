package net.ciderpunk.ld38.resources.interfaces;

import net.ciderpunk.ld38.resources.ResourceManager;

public interface IResourceUser {
	void preLoad(ResourceManager resMan);
	void postLoad(ResourceManager resMan);
}
