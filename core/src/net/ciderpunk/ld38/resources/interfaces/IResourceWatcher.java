package net.ciderpunk.ld38.resources.interfaces;

public interface IResourceWatcher extends net.ciderpunk.ld38.resources.interfaces.IResourceUser {
  void loadComplete();
}
