package net.ciderpunk.ld38.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Disposable;
import net.ciderpunk.ld38.Ld38Game;
import net.ciderpunk.ld38.controls.Action;
import net.ciderpunk.ld38.controls.interfaces.IActionConsumer;
import net.ciderpunk.ld38.entities.blufor.Player;
import net.ciderpunk.ld38.entities.decorations.Explosion;
import net.ciderpunk.ld38.entities.missile.Bullet;
import net.ciderpunk.ld38.resources.interfaces.IResourceUser;
import net.ciderpunk.ld38.resources.ResourceManager;
import net.ciderpunk.ld38.ui.interfaces.IGameManager;
import net.ciderpunk.ld38.level.Level;
import net.ciderpunk.ld38.level.interfaces.ILevel;

import static com.badlogic.gdx.Gdx.input;

/**
 * Created by Matt on 22/04/2017.
 */
public class GameView implements net.ciderpunk.ld38.ui.interfaces.IView, Disposable, IResourceUser, IActionConsumer {

  boolean paused = false;
  boolean debugView = false;
  SpriteBatch batch;
  public final IGameManager owner;
  ILevel level;


  public void loadLevel(String path){
    if (level != null) level.dispose();
    level = new Level(path, this);
    owner.getResMan().addResourceUser(level);
  }

  public GameView(IGameManager owner){
    this.owner = owner;
    batch = new SpriteBatch();
    loadLevel("maps/test.tmx");

    actionDebugView.addConsumer(this);
    actionPause.addConsumer(this);
  }

  @Override
  public void CleanUp() {

  }

  @Override
  public void Init() {
    input.setInputProcessor(Ld38Game.keymap);
  }

  @Override
  public void Draw() {
    Gdx.gl.glClearColor(0, 0, 0, 1);
    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    //batch.begin();
    level.draw(batch, debugView);
    //batch.draw(img, 0, 0);
    //batch.end();
  }

  @Override
  public void Update(float dT) {
    if (!paused){
      //do update
      level.update(dT);
    }

  }

  @Override
  public void dispose() {
    level.dispose();
    batch.dispose();
  }

  @Override
  public void preLoad(ResourceManager resMan) {
    resMan.addResourceUser(new IResourceUser[]{ new Player.Loader(), new Bullet.Loader(), new Explosion.Loader()});
  }

  @Override
  public void postLoad(ResourceManager resMan) {

  }

  @Override
  public boolean startAction(Action action) {
    switch(((GameAction)action).action) {
      case Pause:
        paused = !paused;
        break;
      case ToggleDebug:
        debugView = ! debugView;
    }
    return true;
  }

  @Override
  public boolean endAction(Action action) {
    return true;
  }

  protected enum GameActions{
    Pause,
    ToggleDebug,
  }

  public static class GameAction extends Action{
    GameActions action;
    public GameAction(String name, String key, int defaultKeyCode, GameActions action) {
      super(name, key, defaultKeyCode);
      this.action = action;
    }
  }

  private static final GameAction actionPause = new GameAction("Pause", "pause", Input.Keys.ESCAPE, GameActions.Pause);
  private static final GameAction actionDebugView = new GameAction("ToggleDebug", "toggleDebug", Input.Keys.F1, GameActions.ToggleDebug);
  static {
    Ld38Game.keymap.registerActions(new GameAction[]{actionPause, actionDebugView});
  }
}
