package net.ciderpunk.ld38.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Disposable;
import net.ciderpunk.ld38.resources.ResourceManager;
import net.ciderpunk.ld38.resources.interfaces.IResourceUser;

/**
 * Created by Matt on 22/04/2017.
 */
public class Loader implements net.ciderpunk.ld38.ui.interfaces.IView, Disposable, IResourceUser {
  boolean loaded = false;
  SpriteBatch batch;
  Texture splash;
  net.ciderpunk.ld38.ui.interfaces.IGameManager owner;

  public Loader(net.ciderpunk.ld38.ui.interfaces.IGameManager owner){
    this.owner = owner;
    batch = new SpriteBatch();
  }

  @Override
  public void CleanUp() {

  }

  @Override
  public void Init() {

  }

  @Override
  public void Draw() {
    //clear the screen
    Gdx.gl.glClearColor(0, 0, 0, 1);
    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

    if (loaded){
      batch.begin();
      batch.draw(splash, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
      batch.end();
    }
  }

  @Override
  public void Update(float dT) {

  }

  @Override
  public void dispose() {
    batch.dispose();
  }

  @Override
  public void preLoad(ResourceManager resMan) {
    resMan.load("loading.png", Texture.class);
  }

  @Override
  public void postLoad(ResourceManager resMan) {
    splash = resMan.get("loading.png", Texture.class);
    loaded = true;
  }

}
