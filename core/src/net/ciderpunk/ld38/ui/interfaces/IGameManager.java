package net.ciderpunk.ld38.ui.interfaces;

import net.ciderpunk.ld38.resources.ResourceManager;
import net.ciderpunk.ld38.ui.Views;

/**
 * Created by Matt on 22/04/2017.
 */
public interface IGameManager {
  ResourceManager getResMan();
  void SwitchView(Views view);

}
