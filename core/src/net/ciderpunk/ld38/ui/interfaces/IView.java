package net.ciderpunk.ld38.ui.interfaces;

/**
 * Created by Matt on 22/04/2017.
 */
public interface IView {

  //called when withdrawn
  void CleanUp();
  //called when first displayed
  void Init();

  void Draw();
  void Update(float dT);

}
