package net.ciderpunk.ld38.utils;

/*******************************************************************************
 * Copyright 2011 See AUTHORS file.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/


import com.badlogic.gdx.utils.Pool;

/** A simple linked list that pools its nodes.
 * @author mzechner */
public class PooledLinkedListIterable<T> {

	public final class Iterator{
		private final PooledLinkedListIterable<T> owner;
		private int version;
		private Item<T> iter;
		private Item<T> curr;

		public Iterator(PooledLinkedListIterable<T> owner){
			this.owner = owner;
			resetHead();
		}

		/** Resets itterator to tail
		 *
		 */
		public void resetTail(){
			this.version = this.owner.version;
			this.iter = owner.head;
			this.curr = null;
		}

		/** Resets itterator to head
		 *
		 */
		public void resetHead(){
			this.version = this.owner.version;
			this.iter = owner.head;
			this.curr = null;
		}


		public void reset(){
			resetHead();
		}

		/** Gets the next item in the list
		 *
		 * @return the next item in the list or null if there are no more items */
		public T next () {
			//assert(this.version == owner.version);
			if (iter == null) return null;
			curr = iter;
			iter = iter.next;
			return curr.payload;
		}

		/** Gets the previous item in the list
		 *
		 * @return the previous item in the list or null if there are no more items */
		public T previous () {
			//assert(this.version == owner.version);
			if (iter == null) return null;
			curr = iter;
			iter = iter.prev;
			return curr.payload;
		}

		public void Remove(){
			assert(this.version == owner.version);
			if (curr == null){
				throw new IllegalStateException();
			}
			this.owner.remove(curr);
			this.version++;
		}
	}

	static final class Item<T> {
		public T payload;
		public Item<T> next;
		public Item<T> prev;
	}

	public Iterator GetIterator(){
		return new Iterator(this);
	}


	private Item<T> head;
	private Item<T> tail;
	private Item<T> iter;
	private Item<T> curr;
	private int size = 0;
	private int version = 0;

	private final Pool<Item<T>> pool;

	public PooledLinkedListIterable (int maxPoolSize) {
		this.pool = new Pool<Item<T>>(16, maxPoolSize) {
			@Override
			protected Item<T> newObject () {
				return new Item<T>();
			}
		};
	}

	/** Adds the specified object to the end of the list regardless of iteration status */
	public void add (T object) {
		Item<T> item = pool.obtain();
		item.payload = object;
		item.next = null;
		item.prev = null;
		size++;
		version++;
		if (head == null) {
			head = item;
			tail = item;
			return;
		}
		item.prev = tail;
		tail.next = item;
		tail = item;
	}

	/** Returns the number of items in the list */
	public int size () {
		return size;
	}

	/** Removes the current list item based on the iterator position. */
	private void remove (Item<T> node) {
		version++;
		if (node == null) return;
		size--;
		pool.free(node);


		Item<T> c = node;
		Item<T> n = node.next;
		Item<T> p = node.prev;
		curr = null;

		if (size == 0) {
			head = null;
			tail = null;
			return;
		}

		if (c == head) {
			n.prev = null;
			head = n;
			return;
		}

		if (c == tail) {
			p.next = null;
			tail = p;
			return;
		}

		p.next = n;
		n.prev = p;
	}
}